# 行政区划-数据库

#### 项目介绍
全国行政区划，省市区镇四级，包含名称、完整名称、经纬度、区号、邮编、行政区划代码、拼音。

#### 使用说明

1. area.xlsx    Excel文件数据
2. area.sql     MySQL脚本
3. area.psc     Navicat for MySQL备份文件
4. area.nb3     Navicat Premium备份文件

#### 数据来源

1. 高德API
2. 民政部官网
3. 中国邮政官网
